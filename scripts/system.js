
function getListFromObject (obj){
	var list = [];
	for(var key in obj){
		if (obj.hasOwnProperty(key)){
			list.push({key:key, value:obj[key]});
		}
	}
	return list;
}

var EVL_Monitor_System = Class.extend({
	init: function(app, options){
		options || (options = {});
		this.sessionCreated = false;
		this.numberOfApplicationsShown = options.numberOfApplications || 5;
		this.masterApp = app;
		this.programRunningTable = null;
		this.programRunningPerfTable = null;
		this.data = {
			programRunData : null,
			systemMemory: 0,
			systemUptime: 0,
			processorTable: null,
			dataIndicator : 0,
			storageTable: null
		};
		this.historyData = [];
		this.drawCallback = null;
		Object.observe(this.data, function(changes){

			for(var i=0;i<changes.length;i++){
				if(changes[i].name === "dataIndicator" && changes[i].object.dataIndicator > 4 && this.drawCallback !== null){
					//console.log(changes[i]);
					this.mergeProgramRunData();
					var data = {
						cpuTime: this.data.programRunData.cpuTime,
						memoryUsage: this.data.programRunData.memoryUsage,
						programList: getListFromObject(this.data.programRunData.programtable),
						systemMemory: this.data.systemMemory,
						systemUptime: this.data.systemUptime,
						processorTable: this.data.processorTable,
						storageTable: this.data.storageTable
					};
					this.drawCallback(data);
					this.historyData.push(data);
					this.drawCallback = null;	
				}
			}
		}.bind(this));
	},
	initSNMPSession: function(){
		var host = "localhost";
		var community = "public";
		this.masterApp.sendSNMPRequest("createSession", {host:host, community:community}, function(error, data){
			console.log(data);
			this.sessionCreated = true;
			//this.getCPUData();
		}.bind(this));

	},
	getData: function(drawCallback){
		if (this.sessionCreated === false) return;
		this.data.dataIndicator = 0;
		this.drawCallback = drawCallback;
		this.getProgramRunningTable();
		this.getCPULoadAndStorage();
		this.getDiskUsage();
		var excess = this.historyData.length - (this.masterApp.historyLimit-1);
		if (excess > 0){
			this.historyData.splice(0,excess);
		}
	},
	getHistory: function(){
		return this.historyData;
	},
	
	computeProgramPerfPercentage: function(){
		var temp;
		var tempPrev;
		var i;
		var prevProgTable;
		var prevCpu;
		var prevData = (this.historyData.length > 0)? this.historyData.slice(-1): null;
		if (prevData !== null){
			prevProgTable = prevData.programRunData.programtable;
		}
		
		var programtable = this.data.programRunData.programtable;
		for(i in programtable){
			if (prevProgTable !== null && prevProgTable !== undefined && prevProgTable.hasOwnProperty(i)){
				prevCpu = prevProgTable[i]["cpu"];	
			}
			else{
				prevCpu = 0;
			}
			if (programtable.hasOwnProperty(i)){
				temp = programtable[i];
				temp["cpu"] = (temp["cpu"] - prevCpu) * this.masterApp.state.pullRate;
			}
		}
	},

	mergeProgramRunData: function(){
		for (var key in this.programRunningTable){
			var program = this.programRunningTable[key];
			program["mem"] = parseInt(0.9 + this.programRunningPerfTable[key][2] / 1024.0);
			program["cpu"] = parseInt(0.9 + this.programRunningPerfTable[key][1] / 100.0);
		}
		
		this.data.programRunData = this.filterProgramDataOnType(this.programRunningTable);
		//this.computeProgramPerfPercentage();
		this.programRunningTable = null;
		this.programRunningPerfTable = null;
	},
	getProgramRunningTable: function(){
		var softwareRunTable = "1.3.6.1.2.1.25.4.2";
		var softwarePerfTable = "1.3.6.1.2.1.25.5.1";
		this.masterApp.sendSNMPRequest("getTableColumns", {oid: softwareRunTable, columns:[1,2,6,7]}, function(error, data){
			if (error!== null){
				console.log(error);
			}
			else{ 
				this.programRunningTable = data;
				//console.log(data);
				this.data.dataIndicator = this.data.dataIndicator + 1;
			}
		}.bind(this));

		this.masterApp.sendSNMPRequest("getTable", softwarePerfTable, function(error, data){
			if (error!== null){
				console.log(error);
			}
			else{ 

				this.programRunningPerfTable = data;
				//console.log(data);
				this.data.dataIndicator = this.data.dataIndicator + 1;
			}
		}.bind(this));
	},

	getCPULoadAndStorage: function(){
		var storage = "1.3.6.1.2.1.25.2.2.0";
		var SystemUpTime = "1.3.6.1.2.1.25.1.1.0";
		var processorTable = "1.3.6.1.2.1.25.3.3";  //"1.3.6.1.2.1.25.3.3";
		
		var oid = [storage,SystemUpTime];
		
		this.masterApp.sendSNMPRequest("get", oid, function(error, data){
			if (error!== null){
				console.log(error);
			}
			else{ 
				this.data.systemMemory = parseInt( 0.8 + data[0].value / 1024.0);
				this.data.systemUptime = getHumanReadableTime(data[1].value);
				this.data.dataIndicator = this.data.dataIndicator + 1;
			}
		}.bind(this));
		this.masterApp.sendSNMPRequest("getTable", processorTable, function(error, data){
			if (error!== null){
				console.log(error);
			}
			else{ 
				this.data.processorTable = getListFromObject(data);
				this.data.dataIndicator = this.data.dataIndicator + 1;
			}
		}.bind(this));


	},
	getDiskUsage: function () {
		var storageTable = "1.3.6.1.2.1.25.2.3";
		this.masterApp.sendSNMPRequest("getTable", storageTable, function(error, data){
			if (error!== null){
				console.log(error);
			}
			else{ 
				this.data.storageTable = getListFromObject(data);
				this.data.dataIndicator = this.data.dataIndicator + 1;
			}
		}.bind(this));

	},
	filterProgramDataOnType: function(programtable){
		var typeIdx = "6";
		var typeUnknown = 1;
		var typeSystem = 2;
		var typeDeviceDriver = 3;
		var typeApplication = 4;

		var memoryUsage = {
			system: 0,
			unknown: 0,
			deviceDriver: 0,
			application: 0,
			total: 0
		};
		var cpuTime = {
			system: 0,
			unknown: 0,
			deviceDriver: 0,
			application: 0,
			total: 0
		};
		for (var key in programtable){
			if (programtable.hasOwnProperty(key)){
				var program = programtable[key];
				switch (program[typeIdx]){
					case typeUnknown:
						memoryUsage.unknown += program.mem;
						cpuTime.unknown += program.cpu;
						delete programtable[key];
						break;
					case typeSystem:
						memoryUsage.system += program.mem;
						cpuTime.system += program.cpu;
						delete programtable[key];
						break;
					case typeDeviceDriver:
						memoryUsage.deviceDriver += program.mem;
						cpuTime.deviceDriver += program.cpu;
						delete programtable[key];
						break;
					case typeApplication:
						memoryUsage.application += program.mem;
						cpuTime.application += program.cpu;
						break;
				}
			}
		}
		memoryUsage.total = memoryUsage.unknown + memoryUsage.system + memoryUsage.deviceDriver + memoryUsage.application;
		cpuTime.total = cpuTime.unknown + cpuTime.system + cpuTime.deviceDriver + cpuTime.application;
		return {memoryUsage:memoryUsage, cpuTime: cpuTime, programtable: programtable};

	}

});