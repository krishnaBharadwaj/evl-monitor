var EVL_Monitor_Energy = Class.extend({
	init: function (app, options) {
		// body...
		this.masterApp = app;
		this.currentData = null;
		this.historyData = [];
		this.historicMaximums = {
			totalInputPower: 0,
			accumulatedEnergy: 0,
			current: 0,
		};
	},
	getData: function(drawCallback) {
		this.drawCallback = drawCallback;
		readFile("http://pandora.evl.uic.edu:8888/EmersonPDU",this.makeEnergyDataObjectList.bind(this), "JSON");
	},
	makeEnergyDataObjectList:function(error, data) {
		var lst = [];
		for (var i=0;i<data.val[0].length; i++) {
			var node = {};
			node.totalInputPower = data.val[0][i];
			node.accumulatedEnergy = data.val[1][i];
			node.branchCurrent = [];
			node.branchCurrent.push(data.val[2][i*3 + 0]);
			node.branchCurrent.push(data.val[2][i*3 + 1]);
			node.branchCurrent.push(data.val[2][i*3 + 2]);
			lst.push(node);
		}
		this.historicMaximums.totalInputPower = Math.max(...data.val[0], this.historicMaximums.totalInputPower);
		this.historicMaximums.accumulatedEnergy = Math.max(...data.val[1], this.historicMaximums.accumulatedEnergy);
		this.historicMaximums.current = Math.max(...data.val[2], this.historicMaximums.current);
		this.currentData = lst;
		this.historyData.push(this.currentData);
		var excess = this.historyData.length - (this.masterApp.historyLimit-1);
		if (excess > 0){
			this.historyData.splice(0,excess);
		}
		//console.log(lst);
		this.drawCallback({data:this.currentData, max:this.historicMaximums});
	}
});