function getListFromObject (obj){
	var list = [];
	for(var key in obj){
		if (obj.hasOwnProperty(key)){
			list.push({key:key, value:obj[key]});
		}
	}
	return list;
}

var EVL_Monitor_SAGE2App = Class.extend({
	init: function(app, options){
		options = options || {};
		this.masterApp = app;		
		this.appDictionary = {};
		this.historyData = [];
	},
	addEventData: function(event_data){
		/*if (this.appDictionary.hasOwnProperty(event_data.id) === false){
			this.appDictionary[event_data.id] = {};
		}
		if (this.appDictionary[event_data.id].hasOwnProperty(event_data.user.id) === false){
			this.appDictionary[event_data.id][event_data.user.id] = {color:event_data.user.color, eventList: []};
		}
		this.appDictionary[event_data.id][event_data.user.id].eventList.push({type:event_data.type, date:event_data.date})
		*/
		this.historyData.push(event_data);
	},
	updateHistoryData: function(date){
		var excess = this.historyData.length;
		for (var i=0;i<this.historyData.length;i++){
			if(this.historyData[i].date - date > 0){
				excess = i;
				break;
			}
		}
		this.historyData.splice(0,excess);
	},

	getEventHistory: function(){
		return this.historyData;
	}

});