var dll = Class.extend({
	init: function (defaultItem) {
		this.defaultItem = defaultItem;
		this.itemArray = [];
		this.index = 0;
		this.navIndex = 0;
		this.addItemAndGoForward(this.defaultItem);
	},
	addItem: function(item) {
		this.itemArray.push(item);
	},
	removeItem: function() {
		return this.itemArray.pop();
	},
	reset: function() {
		this.itemArray = [];
		this.addItemAndGoForward(this.defaultItem);
	},
	goBackward: function () {
		if (this.navIndex > 0){
			this.navIndex = this.navIndex -1;
		}
		return this.itemArray[this.navIndex];
	},
	goForward: function () {
		if (this.navIndex < this.itemArray.length){
			this.navIndex = this.navIndex + 1;	
		}
		return this.itemArray[this.navIndex];
	},
	prune:function() {
		this.itemArray.splice(this.navIndex+1,this.itemArray.length - this.navIndex - 1);
	},
	addItemAndGoForward: function(item) {
		this.addItem(item);
		this.goForward();
	}
});