//
// SAGE2 application: evlmonitor
// by: Krishna Bharadwaj <kbhara5@uic.edu>
//     Samuel Flores <sflore3@uic.edu>
//
// Copyright (c) 2015
//


function addCSS( url, callback ) {
    var fileref = document.createElement("link")
    if( callback ) fileref.onload = callback;
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", url)
    document.head.appendChild( fileref );
}

function getHumanReadableTime(timeticks){
    var timeObj = {
        days : parseInt(timeticks / 8640000)
    };
    var remainder = timeticks - timeObj.days * 8640000;
    timeObj.hours = parseInt(remainder / 360000);
    remainder = remainder - timeObj.hours * 360000;
    timeObj.minutes = parseInt(remainder / 6000);
    remainder = remainder - timeObj.minutes * 6000;
    timeObj.seconds = parseInt(remainder/100);
    timeObj.str = timeObj.days + " days, " + timeObj.hours + " hours, " + timeObj.minutes + " minutes, " + timeObj.seconds + " seconds";
    return timeObj;
}

// Source: http://stackoverflow.com/questions/4878756/javascript-how-to-capitalize-first-letter-of-each-word-like-a-2-word-city
function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

var _app = null;
var evlmonitor = SAGE2_App.extend( {
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "onfinish";
		this.moveEvents   = "onfinish";

		_app = this;

		addCSS(this.resrcPath + "scripts/style.css");

        this.mainButtonFontFactor   = 18 / 250;    // ~ 18 pixels
        this.submenuFontFactor      = 15 / 250;    // ~ 15 pixels
        this.homeButtonFontFactor   = 11 / 250;    // ~ 11 pixels
        this.menuHeadingFontFactor  = 12 / 250;    // ~ 12 pixels
        this.clusterFontFactor      = 9.5 / 250;   // ~ 9 pixels

        // Should keep track of buttons in app
        //this.buttons = {};
        //this.childButtonsBuilt = {};

        // SAGE2 Application Settings
        this.appInit();
        // Control the frame rate for an animation application
        this.maxFPS = this.state.pullRate;

		this.NetworkNode = null;
		this.switchData = [];
		this.switchIndex = 0;
		this.summaryData = null;
		this.individualData = null;

        this.controls.addButton({label: "Cycle", position: 4, identifier: "cycle"});
        this.controls.addButton({label: "P/BW", position: 1, identifier: "packets"})

        
		this.controls.finishedAddingControls();
		this.enableControls = true;
		this.setUpGraphArea();
		this.system = new EVL_Monitor_System();
		this.system.init(this, {numberOfApplications: this.state.numberOfApplications});
		this.system.initSNMPSession();

		this.requestAppMonitoring();
		this.appMonitor = new EVL_Monitor_SAGE2App();
		this.appMonitor.init(this, null);
		this.fontScaleFactor = 9.0 / this.getWidth(); // 11 px
		this.buildNetworkTree();
        this.displayDataElementName = "home";

		this.system.getData(function(data){
			console.log(data);
            this.systemData = data;
		}.bind(this));

		this.eventTrigger = false;
		//program button linker
		this.selectedProgramNumber = 0;
        this.navigationHandle = new dll();
        this.navigationHandle.init("home");
        this.energy = new EVL_Monitor_Energy();
        this.energy.init(this, {numberOfApplications: this.state.numberOfApplications});
        this.energy.getData(function(data) {
            this.energyData = data ;
        }.bind(this));

        this.counter = 0;
        this.slideShowCounter = 0;
        this.slideShowToggle = false;

        this.showPackets = true;
	},

    appInit: function() {
        

        this.width = this.element.clientWidth;
        this.height = this.element.clientHeight;
        this.margin = Math.min(this.width, this.height) * 0.1;
        var box =  "0,0, 1000," + parseInt(1000*(this.height/this.width));
        this.svg = d3.select(this.element).append("svg")
            .attr("class", "svgCanvas")
            .attr("viewbox", box)
            .attr("preserveAspectRatio", "xMinYMin meet");

        //Network data related variables
        this.showData = null;
        this.historyData = [];
        this.historyLimit = this.state.historyLimit;
        this.energyData = null;
        this.systemData = null;
    },

	load: function(date) {
		console.log('evlmonitor> Load with state value', this.state.value);
		this.eventTriggeredRefresh(date);
	},

	draw: function(date) {
		if (this.eventTrigger !== true){
			this.buildNetworkTree(date);
			this.system.getData(function(data){
				this.systemData = data ;
				//console.log(data);
			}.bind(this));
            this.energy.getData(function(data) {
                this.energyData = data ;
            }.bind(this));
			var historyDate = date - (1000*this.historyLimit/this.state.pullRate);
			this.appMonitor.updateHistoryData(historyDate);
            if (this.slideShowToggle === true) {
                if (this.counter>3){
                    this.slideShow();
                    this.counter = 0;
                } else {
                    this.counter = this.counter + 1;
                }
            }
            

		}
		var showData = null;
		if (this.displayDataElementName !== null && this.displayDataElementName !== undefined){
            this.wipeItAll();
            if (this.displayDataElementName === "home") {
                this.drawHome(date);
            } else {
                this.drawNavigationButtons();
                if (this.displayDataElementName === "SAGE2Apps") {
                    this.drawSAGE2AppsData(date);
                } else if (this.displayDataElementName === "system") {
                    this.drawSystemData();
                } else if (this.displayDataElementName === "energy") {
                    this.drawEnergyChart();
                } else if(this.NetworkNode !== null) {
                    showData = this.searchNode(this.NetworkNode,this.displayDataElementName);
                    this.drawChart(showData);   
                }
            }
			
		}
		//console.log(this.historyData.length);
		this.eventTrigger = false;
	},

    resize: function(date) {
        this.width = this.element.clientWidth;
        this.height = this.element.clientHeight;
        this.margin = Math.min(this.width, this.height) * 0.1;
        var box =  "0,0, 1000," + parseInt(1000*(this.height/this.width));
        this.svg.attr("viewbox", box)
        this.refresh(date);
    },

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// Call handler function
			//this.handleMouseDown(position, data, date);
            if (this.displayDataElementName !== "home") {
    			this.checkProgramClicked(position, date);
    			this.checkNodeButtonClicked(position, date);
                this.checkNavigationButtonClicked(position, date);
            } else {
                this.checkHomeScreenClickablesClicked(position, date);
            }

		}
		else if (eventType === "pointerMove" && this.dragging) {
		}
		else if (eventType === "pointerRelease" && (data.button === "left")) {
			// Call handler function
			//this.handleMouseUp(position, data, date);
		}

		// Scroll events for zoom
		else if (eventType === "pointerScroll") {
		}
		else if (eventType === "widgetEvent"){
            switch(data.identifier) {
                case "cycle":
                    this.slideShowToggle = !this.slideShowToggle;
                    break;
                case "packets":
                	this.showPackets = !this.showPackets;
                	break;
            }
            this.eventTriggeredRefresh(date);
		}
		else if (eventType === "keyboard") {
			switch(data.character){
				case "l":
					this.displayDataElementName = "lyra nodes";
					break;
				case "o":
					this.displayDataElementName = "orion nodes";
					break;
				case "1":
					this.displayDataElementName = "switch1";
					break;
				case "2":
					this.displayDataElementName = "switch2";
					break;
				case "m":
					this.displayDataElementName = "mx480 et nodes";
					break;
				case "e":
					this.displayDataElementName = "energy";
					break;
				case "s":
					this.displayDataElementName = "system";
					break;
				case "r":
					this.displayDataElementName = "SAGE2Apps";
					break;
				default:
					break;

			}
			
			this.eventTriggeredRefresh(date);
		}
		else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") { // left
				this.eventTriggeredRefresh(date);
			}
			else if (data.code === 38 && data.state === "down") { // up
				this.eventTriggeredRefresh(date);
			}
			else if (data.code === 39 && data.state === "down") { // right
				this.eventTriggeredRefresh(date);
			}
			else if (data.code === 40 && data.state === "down") { // down
				this.eventTriggeredRefresh(date);
			}
		}
		else if (eventType === "monitor") {
			//console.log(data.user);
			this.appMonitor.addEventData(data);
		}
	},
	eventTriggeredRefresh: function(date){
		this.eventTrigger = true;
		this.refresh(date);
	},
	buildNetworkTree: function(date){
		if (this.switchData[0] !== null && this.switchData[0] !== undefined && this.switchData[1] !== null && this.switchData[1] !== undefined){
			var NetworkNode = {};
			NetworkNode.Alias = "Network";
			NetworkNode.label = "Network";
			NetworkNode.PacketsIN = 0;
			NetworkNode.PacketsOUT = 0;
			NetworkNode.DOWNLINKRate = 0;
			NetworkNode.UPLINKRate = 0;
			NetworkNode.Operationalstatus = "down";
			NetworkNode.children = [];
			for(var i=0;i<this.switchData.length;i++){
				NetworkNode.PacketsIN += this.switchData[i].PacketsIN;
				NetworkNode.PacketsOUT += this.switchData[i].PacketsIN;
				NetworkNode.UPLINKRate += this.switchData[i].UPLINKRate;
				NetworkNode.DOWNLINKRate += this.switchData[i].DOWNLINKRate;
				if (NetworkNode.Operationalstatus === "down"){
					NetworkNode.Operationalstatus = this.switchData[i].Operationalstatus;
				}
				NetworkNode.children.push(this.switchData[i]);

			}
			this.NetworkNode = NetworkNode;
			this.historyData.push({date:date,Network:NetworkNode});
			var excess = this.historyData.length - this.historyLimit;
			if (excess > 0){
				this.historyData.splice(0,excess);
			}
		}
		readFile("http://pandora.evl.uic.edu:8888/AristaSwitch1",this.getSwitch1Data.bind(this), "JSON");
		readFile("http://pandora.evl.uic.edu:8888/AristaSwitch2",this.getSwitch2Data.bind(this), "JSON");
	},
	getSwitch1Data: function(error, data){
		this.switchData[0] = this.buildSwitchNode(data, 1, {"":true, "Empty [bad]":true});
	},
	getSwitch2Data: function(error, data){
		this.switchData[1] = this.buildSwitchNode(data, 2, {});
	},
	buildSwitchNode: function(data, index, ignoreAliasDict){
		var leafNodeList = this.buildLeafNodes(data).filter(function(d){ return !(ignoreAliasDict[d.Alias] === true);});
		var switchNode = {};
		switchNode.Alias = "switch" + index;
		switchNode.label = "switch" + index;
		switchNode.PacketsIN = 0;
		switchNode.PacketsOUT = 0;
		switchNode.DOWNLINKRate = 0;
		switchNode.UPLINKRate = 0;
		switchNode.children = [];
		var parentNode = {};
		//var componentNodeExp = new RegExp("\w+-\d+");
		for (var i=0;i<leafNodeList.length;i++){
			var node = leafNodeList[i];
			node.label = node.Alias;
			var hyphenAt = node.Alias.indexOf("-");
			if (hyphenAt > -1){
				var parentName = node.Alias.substr(0,hyphenAt) + " nodes";
				if (parentNode.hasOwnProperty(parentName) === false){
					parentNode[parentName] = {};
					parentNode[parentName].Alias = parentName;
					parentNode[parentName].label = parentName;
					parentNode[parentName].PacketsIN = 0;
					parentNode[parentName].PacketsOUT = 0;
					parentNode[parentName].DOWNLINKRate = 0;
					parentNode[parentName].UPLINKRate = 0;
					parentNode[parentName].Operationalstatus = "down";
					parentNode[parentName].children = [];
				}
				node.label = node.label.split("-").filter(function(d, i){ return i>0;}).join("-");
				parentNode[parentName].PacketsIN += node.PacketsIN;
				parentNode[parentName].PacketsOUT += node.PacketsOUT;
				parentNode[parentName].DOWNLINKRate += node.DOWNLINKRate;
				parentNode[parentName].UPLINKRate += node.UPLINKRate;
				if (parentNode[parentName].Operationalstatus === "down"){
					parentNode[parentName].Operationalstatus = node.Operationalstatus;
				}
				parentNode[parentName].children.push(node);
				
			}
			else {
				parentNode[node.Alias] = node;
			}
		}
		//var intermediateNode = {};

		for (var j in parentNode){
			if (parentNode.hasOwnProperty(j)===true){
				switchNode.PacketsIN += parentNode[j].PacketsIN;
				switchNode.PacketsOUT += parentNode[j].PacketsOUT;
				switchNode.DOWNLINKRate += parentNode[j].DOWNLINKRate;
				switchNode.UPLINKRate += parentNode[j].UPLINKRate;
				switchNode.children.push(parentNode[j]);
			}
		}
		//console.log(switchNode);
		return switchNode;
	},
	buildLeafNodes: function(data){
		var i, j;
		var leafNodeList = [];
		var networkUnits = data.units;
		/*if (this.leafNodeList === null || this.leafNodeList === undefined){
			this.leafNodeList = [];	
		}
		if (this.networkUnits === null || this.networkUnits === undefined){
			this.networkUnits = 
		}*/
		for(i=0;i<data.val.length;i++){
			var e = data.val[i];
			for(j=0;j<e.length;j++){
				if (leafNodeList[j] === null || leafNodeList[j] === undefined){
					leafNodeList.push(this.getNewNodeObject(networkUnits));
				}
				leafNodeList[j][networkUnits[i]] = e[j];
			}
		}
		return leafNodeList;
		//console.log(this.leafNodeList);
	},
	getNewNodeObject: function(networkUnits){
		var nodeObject = {};
		for (var i=0;i<networkUnits.length;i++){
			var unit = networkUnits[i];
			if (unit.indexOf("(") > -1){
				unit = unit.split(" (")[0];
				networkUnits[i] = unit;
			}
			if(unit.indexOf(" ") > -1){
				unit = unit.replace(/\s*/g, "");
				networkUnits[i] = unit;
			}
			nodeObject[networkUnits[i]] = null;
		}
		return nodeObject;
	},
	setUpGraphArea: function(){
		
		this.graphBackground = this.svg.append("rect")
			.attr("x", "0%")
			.attr("y", "0%")
			.attr("width", "100%")
			.attr("height", "100%")
			.attr("fill", "rgb(40,40,40)");
			// .attr("stroke", "rgb(230,230,230");
		
		this.smallChart = this.svg.append("rect")
			.attr("x", "4%")
			.attr("y", "20%")
			.attr("width", "92%")
			.attr("height","60%")
			.attr("class", "smallChart")
			.attr("fill", "rgb(20,20,20)")
			.attr("stroke", "rgb(10,10,10)")
			.attr("visibility", "hidden");
		this.largeChart = this.svg.append("rect")
			.attr("x", "4%")
			.attr("y", "5%")
			.attr("width", "92%")
			.attr("height","90%")
			.attr("class", "largeChart")
			.attr("fill", "rgb(20,20,20)")
			.attr("stroke", "rgb(10,10,10)")
			.attr("visibility", "hidden");
		this.graphMargin = 10.0;
		this.graphLeft = 10.0;
		this.graphWidth = 84.0;
		this.graphHeight = 38.0;
		this.graphAreaHeight = 90.0;
		this.graphAreaWidth = 92.0;
		this.graphAreaTop = 5.0;
		this.graphAreaLeft = 4.0;
		this.graphTop1 = 14.0;
		this.graphTop2 = 50.0;
		this.graphTopSingle = 27.0;
		this.graphHeightSingle = 46.0;
		
		this.multiChartBars = this.svg.selectAll(".multiChartBar");
		this.multiChartAxis = d3.svg.axis()
			.orient("bottom");
		this.colorScale = d3.scale.ordinal()
			.domain(["packets in", "packets out", "uplink rate", "downlink rate", "nodeUpLabel", "nodeDownLabel", "nodeUpLabelBorder", "nodeDownLabelBorder"])
			.range(["rgb(170,165,130)", "rgb(170,110,70)", "rgb(170,165,130)", "rgb(170,110,70)", "rgb(82,82,120)", "rgb(117,27,40)", "rgb(92,92,150)", "rgb(127,37,60)"]);
		this.summaryChartYScale = d3.scale.linear()
				.range([0, this.graphHeight*0.6]);

	},

	showLargeChart: function(){
		this.largeChart.attr("visibility", "visible");
		this.smallChart.attr("visibility", "hidden");
	},
	showSmallChart: function(){
		this.smallChart.attr("visibility", "visible");
		this.largeChart.attr("visibility", "hidden");
	},

	
	drawChart: function(node){
		if (node === null || node === undefined) return;
		if(node.hasOwnProperty("children")===true){
			if (this.checkForDataInNode(node) === false){
				this.showSmallChart();
                this.displayNoDataMessage({left:this.graphAreaLeft, width:this.graphAreaWidth, 
                top: this.graphTopSingle, height:this.graphHeightSingle});
			}
			else{
				this.showLargeChart();
				this.drawMultiChart(node);
				this.drawSummaryChart(node, {top:this.graphTop2, height:this.graphHeight});
				this.drawHistory(this.graphTop2, this.graphHeight);
			}
		}
		else{
			this.showSmallChart();
            if (this.checkNodeOnlineStatus(node) === false) {
                this.showSmallChart();
                this.displayOfflineNodeMessage(this.graphTopSingle, this.graphHeightSingle);
            }
			else if (this.checkForDataInNode(node) === false){
				this.displayNoDataMessage({left:this.graphAreaLeft, width:this.graphAreaWidth, 
                top: this.graphTopSingle, height:this.graphHeightSingle});
			}
			else{
				this.drawSummaryChart(node, {top:this.graphTopSingle, height:this.graphHeightSingle});
				this.drawHistory(this.graphTopSingle, this.graphHeightSingle);
			}
		}
	},

	checkForDataInNode: function(node) {
		if (node.PacketsIN > 0 || node.PacketsOUT > 0){
			return true;
		}
		return false;
	},

    checkNodeOnlineStatus: function(node) {
        if (node.Operationalstatus === "up") {
            return true;
        }
        else if (node.Operationalstatus === "down") {
            return false;
        }
    },

	drawMultiChart: function(node, box){
		var array = node.children;
		array.sort(function(a,b){
			var aar = a.Alias.split("-");
			var bar = b.Alias.split("-");

			if (aar[0] > bar[0]) return 1;
			if (aar[0] < bar[0]) return -1;
			if (aar.length > 1 && bar.length > 1){
				if (aar[1] > bar[1]) return 1;
				if (aar[1] < bar[1]) return -1;
			}
			return 0 ;
		});

		var meanLabelLength = d3.mean(array, function(d) {return d.Alias.length;});
		var numberOfelements = array.length;
		var chartLeft = this.graphMargin;
		var chartWidth = this.graphWidth;
		var chartTop = this.graphTop1;
		var chartHeight = this.graphHeight*0.7;
		var buttonWidth = chartWidth/numberOfelements;
		this.multiChartXScale = d3.scale.linear()
				.range([chartLeft, chartLeft + chartWidth]);
		this.multiChartYScale = d3.scale.linear()
				.range([0, chartHeight]);
		this.multiChartXScale.domain([0, array.length]);
		this.multiChartYScale.domain([0, d3.max(array,function(d){
			return this.showPackets ? Math.max(d.PacketsOUT,d.PacketsIN) : Math.max(d.DOWNLINKRate,d.UPLINKRate);}.bind(this))]);
		this.multiChartBars = this.svg.selectAll(".multiChartBar")
			.data(array);
		
		var yScale = d3.scale.linear()
				   	.domain([0, d3.max(array,function(d){
				   		return this.showPackets ? Math.max(d.PacketsOUT,d.PacketsIN) : Math.max(d.DOWNLINKRate,d.UPLINKRate);}.bind(this))])
				   	.range([chartHeight*this.height/100.0, 0]);
		var yAxis = d3.svg.axis()
          	.scale(yScale)
          	.orient("left")
          	.innerTickSize(-chartWidth/100.0 * this.getWidth())
		    .outerTickSize(0)
		    .tickPadding(5)
		    .ticks(5);
 
        this.svg.append("g")
            .attr("class", "axis")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", "0.5rem")
            .attr("transform", "translate(" + (chartLeft*this.getWidth()/100.0) +","+ (chartTop*this.getHeight()/100.0) + ")")
            .call(yAxis)
            .append("text")
			.attr("class", "label")
			.attr("transform", "rotate(-90)")
			.attr("x", -40)
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "start")
			.text(this.showPackets ? "# of packets" : "Bytes/Second");





        var bar = this.multiChartBars.enter()
                .append("g");

        bar.append("rect")
            .attr("x", function(d, i){ return (this.multiChartXScale(i) + buttonWidth/2.0 -0.7 ) + "%"; }.bind(this))
            .attr("y", function(d){ return (chartTop + chartHeight - this.multiChartYScale(this.showPackets? d.PacketsIN : d.UPLINKRate)) + "%"; }.bind(this))
            .attr("width", "0.7%")
            .attr("fill", this.colorScale(this.showPackets? "packets in" : "uplink rate"))
            .attr("height", function(d){ return this.multiChartYScale(this.showPackets? d.PacketsIN : d.UPLINKRate) + "%"; }.bind(this));
        bar.append("rect")
            .attr("x", function(d, i){ return (this.multiChartXScale(i) + buttonWidth/2.0) + "%"; }.bind(this))
            .attr("y", function(d){ return (chartTop + chartHeight - this.multiChartYScale(this.showPackets? d.PacketsOUT : d.DOWNLINKRate)) + "%"; }.bind(this))
            .attr("width", "0.7%")
            .attr("fill", this.colorScale(this.showPackets? "packets out" : "downlink rate"))
            .attr("height", function(d){ return this.multiChartYScale(this.showPackets? d.PacketsOUT : d.DOWNLINKRate) + "%"; }.bind(this));

        var nodeButton = bar.append("g").attr("class", "nodeButton");
        nodeButton.attr("id", function(d){ return d.Alias;})
        	.append("rect")
            .attr("x", function(d, i){ return this.multiChartXScale(i) + "%"; }.bind(this))
            .attr("y", function(d, i){
                return (chartTop + chartHeight+1.0) + "%";
            }.bind(this))
            .attr("width", buttonWidth + "%")
            .attr("fill", function(d){
                return (d.Operationalstatus === "down") ? this.colorScale("nodeDownLabel") : this.colorScale("nodeUpLabel");
            }.bind(this))
            .attr("stroke", function(d){
                return (d.Operationalstatus === "down") ? this.colorScale("nodeDownLabelBorder") : this.colorScale("nodeUpLabelBorder");
            }.bind(this))
            .attr("height", function(d){ return "4%"; }.bind(this));


        nodeButton.append("text")
            .attr("x", function(d, i){ return (this.multiChartXScale(i) + buttonWidth/2.0) + "%"; }.bind(this))
            .attr("y", function(d, i){
                var voffset = (d.Alias.split(" ").length > 1)? 2.5 : 3.5;
                return (chartTop + chartHeight+voffset) + "%";
            }.bind(this))
            .style("text-anchor", "middle")
            .attr("fill", "rgb(235,235,235)")
            .attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")
            .text(function(d) { return d.label.split(" ")[0]; });

        nodeButton.append("text")
            //.filter(function(d){return (d.Alias.split(" ").length > 1);})
            .attr("x", function(d, i){ return (this.multiChartXScale(i) + buttonWidth/2.0) + "%"; }.bind(this))
            .attr("y", function(d, i){
                var voffset = 4.5;
                return (chartTop + chartHeight+voffset) + "%";
            }.bind(this))
            .style("text-anchor", "middle")
            .attr("fill", "rgb(235,235,235)")
            .attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")
            .text(function(d) {
                var parts = d.label.split(" ");
                return parts.filter(function(d,i){return i>0;}).join(" ");
            });

        this.svg.append("g")
			.attr("class", "heading")
			.append("text")
			.attr("x", (this.graphMargin)/100.0 * this.getWidth())
			.attr("y", (chartTop/2.0)* this.getHeight()/100.0)
			.style("text-anchor", "start")
			.attr("fill", "rgb(200,190,210)")
			.attr("font-size", parseInt(1.4*this.fontScaleFactor * this.getWidth()) + "px")	
		    .text( toTitleCase(node.Alias));
        this.addLegend("individual", this.showPackets? this.colorScale.domain().slice(0,2) : this.colorScale.domain().slice(2,4), this.colorScale, {y: chartTop/2.0, x: chartLeft + chartWidth});

    },
    getWidth:function(){
        return this.element.clientWidth;
    },
    getMenuWidth: function() {
        return this.element.clientWidth * 0.25;
    },
    getHeight:function(){
        return this.element.clientHeight;
    },
    displayNoDataMessage: function(box){
    	this.svg.append("g")
			.attr("class", "heading")
			.append("text")
			.attr("x", (box.left + box.width/2.0) * this.getWidth()/100.0)
			.attr("y", (box.top + box.height/2.0) * this.getHeight()/100.0)
			.style("text-anchor", "middle")
			.attr("fill", "rgb(200,190,210)")
			.attr("font-size", parseInt(4.0*this.fontScaleFactor * this.getWidth()) + "px")	
		    .text(box.message || "No data available!");
    },
    displayOfflineNodeMessage: function(top, height) {
        this.svg.append("g")
            .attr("class", "heading")
            .append("text")
            .attr("x", this.getWidth()/2.0)
            .attr("y", (top + height/2.0) * this.getHeight()/100.0)
            .style("text-anchor", "middle")
            .attr("fill", "rgb(200,190,210)")
            .attr("font-size", parseInt(4.0*this.fontScaleFactor * this.getWidth()) + "px")
            .text("Node Offline!");
    },
    drawSummaryChart: function(node, box){
        var numberOfelements = 1;
        var chartLeft = box.left || this.graphMargin;
        var chartWidth = this.graphWidth;
        var chartTop = box.top;
        var chartHeight = box.height*0.7;
        var buttonWidth = chartWidth/numberOfelements;
        //this.summaryChartXScale.domain([0, array.length]);

        this.summaryChartYScale.domain([0, this.showPackets? Math.max(node.PacketsOUT,node.PacketsIN) : Math.max(node.DOWNLINKRate, node.UPLINKRate)]);

        //this.summaryChartBars = this.svg.selectAll(".summaryChartBar");
        // .attr("transform", "translate(" + (chartLeft*this.getWidth()/100.0) +","+ ((this.graphHeight*0.4) *this.getHeight()/100.0) + ")")
        var bar = this.svg.append("g").attr("class", ".summaryChartBar");
        bar.append("rect")
            .attr("x", (chartLeft + 1.5) + "%") // +0.5 + 2.0
            .attr("y", (chartTop + box.height - this.summaryChartYScale(this.showPackets? node.PacketsIN : node.UPLINKRate)) + "%")
            .attr("width", "0.5%")
            .attr("fill", this.colorScale(this.showPackets? "packets in" : "uplink rate"))
            .attr("height", this.summaryChartYScale(this.showPackets? node.PacketsIN : node.UPLINKRate) + "%");
        bar.append("rect")
            .attr("x", (chartLeft + 2.0) + "%") // -0.5 + 2.0
            .attr("y", (chartTop + box.height - this.summaryChartYScale(this.showPackets? node.PacketsOUT : node.DOWNLINKRate)) + "%")
            .attr("width", "0.5%")
            .attr("fill", this.colorScale(this.showPackets? "packets out" : "downlink rate"))
            .attr("height", this.summaryChartYScale(this.showPackets? node.PacketsOUT : node.DOWNLINKRate) + "%");


        bar.append("text")
            .attr("class", "axis")
            .attr("transform", "translate("+ (chartLeft+2.0)/100.0 * this.getWidth() + "," + (chartTop + box.height + 2.0)/100.0 * this.getHeight()+ ")") //" rotate(-65 0 0)")
            //.attr("x",  + "%")
            //.attr("y",  + "%")
            .style("text-anchor", "middle")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", parseInt(1.4*this.fontScaleFactor * this.getWidth()) + "px")	
            //.attr("dx", "-.8em")
            //.attr("dy", ".8em")

            .text(node.Alias);

        var yScale = d3.scale.linear()
                    .domain([0, this.showPackets? Math.max(node.PacketsOUT,node.PacketsIN) : Math.max(node.DOWNLINKRate, node.UPLINKRate)])
                    .range([box.height*0.6*this.getHeight()/100.0, 0]);
        var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient("left")
            .ticks(5);
        this.svg.append("g")
		    .attr("class", "axis")
		    .attr("fill", "rgb(220,220,220)")
			.attr("font-size", "0.5rem")
		    .attr("transform", "translate(" + (chartLeft)/100.0 * this.getWidth() +","+ ((box.top + box.height*0.4) *this.getHeight()/100.0) + ")")
		    .call(yAxis)

		this.svg.append("g")
			.attr("class", "heading")
			.append("text")
			.attr("x", (chartLeft)/100.0 * this.getWidth())
			.attr("y", (chartTop +this.graphMargin/2.0)* this.getHeight()/100.0)
			.style("text-anchor", box.small? "middle" : "start")
            .attr("font-weight", "bold")
            .attr("font-size", parseInt(2.0*this.fontScaleFactor * this.getWidth()) + "px")
            .attr("fill", "rgb(220,220,220)")		    
            .text(box.small? "Network data" : "Summary and History");
		//this.addLegend("summary", this.colorScale.domain().slice(0,2), this.colorScale, {y: top +chartLeft/2.0, x: this.graphWidth + this.graphMargin});
	},
	getHistory: function(nodeName){
		var nodeArray = [];
		for (var i=0; i<this.historyData.length;i++){
			var node = this.searchNode(this.historyData[i].Network,nodeName || this.displayDataElementName) ;						
			if (node!== null){
				nodeArray.push({date:this.historyData[i].date, node:node});
			}
		}
		return nodeArray;
	},
	drawHistory: function(top, height, nodeName){
		var nodeArray = this.getHistory(nodeName);
		
      	var x = d3.scale.linear()
      		.domain([0,nodeArray.length])
    		.range([(this.graphLeft * 2.0), this.graphMargin + this.graphWidth]);

    	var y = d3.scale.linear()
    		.domain([0, d3.max(nodeArray,function(d){
    			return this.showPackets? Math.max(d.node.PacketsOUT,d.node.PacketsIN) : Math.max(d.node.DOWNLINKRate, d.node.UPLINKRate);}.bind(this))])
    		.range([0, height*0.6]);
      	var lineIn = d3.svg.line()
		    .interpolate("basis")
		    .x(function(d,i) { return x(i)/100.0 * this.getWidth(); }.bind(this))
		    .y(function(d) { return (top + height - y(this.showPackets? d.node.PacketsIN : d.node.UPLINKRate))/100.0 * this.getHeight(); }.bind(this));

		var lineOut = d3.svg.line()
		    .interpolate("basis")
		    .x(function(d, i) { return x(i)/100.0 * this.getWidth(); }.bind(this))
		    .y(function(d) { return (top + height - y(this.showPackets? d.node.PacketsOUT : d.node.DOWNLINKRate))/100.0 * this.getHeight(); }.bind(this));

		this.svg.selectAll(".lineIn").remove();
		this.svg.selectAll(".lineOut").remove();
  		this.svg.append("path")
  			.datum(nodeArray)
	      	.attr("class", "lineIn")
	      	.attr("d", lineIn)
	      	.attr("stroke", this.colorScale(this.showPackets? "packets in" : "uplink rate"))
            .attr("stroke-width", "0.3%")
	      	.attr("fill", "none");
	    this.svg.append("path")
	      	.datum(nodeArray)
	      	.attr("class", "lineOut")
	      	.attr("d", lineOut)
	      	.attr("stroke", this.colorScale(this.showPackets? "packets out" : "downlink rate"))
            .attr("stroke-width", "0.3%")
	      	.attr("fill", "none");

	    var xScale = d3.time.scale()
	    			.domain(d3.extent(nodeArray, function(d) { return d.date; }))
				   	.range([(this.graphLeft * 2.0)/100.0 * this.getWidth(), (this.graphMargin + this.graphWidth)/100.0 * this.getWidth()]);
		var xAxis = d3.svg.axis()
          	.scale(xScale)
          	.orient("bottom")
          	.ticks(Math.min(nodeArray.length, 8));

        this.svg.append("g")
            .attr("class", "axis")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", "0.5rem")
            .attr("transform", "translate(" + 0 +","+ ((top + height) *this.getHeight()/100.0) + ")")
            .call(xAxis)
            .append("text")
			.attr("class", "label")
			.attr("x", (this.graphMargin + this.graphWidth)/100.0 * this.getWidth())
			.attr("y", -6)
			.style("text-anchor", "end")
			.text("Clock time");

        var yScale = d3.scale.linear()
                    .domain(y.domain())
                    .range([height*0.6*this.getHeight()/100.0, 0]);
        var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient("left")
            .innerTickSize(-(this.graphMargin - 2.0*this.graphLeft + this.graphWidth)/100.0 * this.getWidth())
            .outerTickSize(0)
            .tickPadding(5)
            .ticks(5);
        this.svg.append("g")
            .attr("class", "axis")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", "0.5rem")
            .attr("transform", "translate(" + ((this.graphLeft * 2.0)/100.0 * this.getWidth()) +","+ ((top + height*0.4) *this.getHeight()/100.0) + ")")
            .call(yAxis)
            .append("text")
			.attr("class", "label")
			.attr("transform", "rotate(-90)")
			.attr("x", -40)
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "start")
			.text(this.showPackets ? "# of packets" : "Bytes/Second");

    },
    searchNode: function(node, name){
        var temp = null;
        if (node.hasOwnProperty(name) === true ){
            return node[name];
        }
        else if (node.hasOwnProperty("Alias") === true && node.Alias === name){
            return node;
        }
        else if (node.hasOwnProperty("children") === true){
            for (var i=0;i<node.children.length && temp === null;i++){
                temp = this.searchNode(node.children[i], name);
            }
        }
        return temp;
    },

    addLegend: function (legendTitle, legendEntries, colorScale, position, padding)
    {
        // Add a legend for the color values.
        this.svg.selectAll(".legend" + legendTitle).remove();
        var legend = this.svg.selectAll(".legend" + legendTitle)
        .data(legendEntries)
        .enter().append("g")
        //.attr("y", lowerHeight)
        .attr("class", "legend");
        /*.attr("transform", function(d, i) {
          return "translate(" +   + "," + (position.y + i * 20) + ")"; 
        });*/
		var height = 1.5;
		padding = padding || height;
		
		var gap = height + padding;
		legend.append("rect")
        .attr("x", (position.x - 1.5) + "%")
        .attr("width", "1.5%")
        .attr("height", height + "%")
        .attr("y", function(d, i){
            return (position.y + gap*i) + "%" ;
        })
        //.style("stroke", "black")
        .style("fill", function(d){
            return colorScale(d);
        });

        legend.append("text")
        .attr("x", (position.x -2.0 )+ "%")
        .attr("y", function(d, i){
            return (position.y + gap*i) + "%" ;
        })
        .attr("dy", "1.0%")
        .style("fill", "rgb(180,180,180)")
        .attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
        .style("text-anchor","end")
        .text(function(d){
            return d;
        });
		
	},

	drawSAGE2AppsData: function(date){
		this.showLargeChart();
		this.drawSAGE2AppsEventHistory(date);
		this.drawSAGE2AppsEventTrigger();
	},

	drawSystemData: function(box){
        if (box !== null && box !== undefined) {
            this.drawProcessorLoad(box);
        } else {
    		this.showLargeChart();
    		//console.log(this.systemData);
    		this.drawApplicationCPUMEMPlot();
    		this.drawProcessorLoad();
    		this.drawRAMStatus();
    		this.drawStorage();
        }
	},
	
	drawStorage: function(box) {

		var storageTable = this.systemData.storageTable.map(function(d){return d.value;});
		
		var disks = storageTable.filter(function(d) { return d["2"].indexOf("1.3.6.1.2.1.25.2.1.4") > -1;});
		var physical = storageTable.filter(function(d) { return d["2"].indexOf("1.3.6.1.2.1.25.2.1.2") > -1;});
		var virtual = storageTable.filter(function(d) { return d["2"].indexOf("1.3.6.1.2.1.25.2.1.3") > -1;});
		physical = physical[0];
		virtual = virtual[0];
		console.log(physical, virtual);
		var totalFree = 0;
		var totalUsed = 0;
		for (var i=0;i<disks.length;i++) {
			disks[i]["cumulativeFree"] = totalFree;
			totalFree = totalFree + parseFloat(disks[i]["4"]) * parseFloat(disks[i]["5"]);
			totalUsed = totalUsed + parseFloat(disks[i]["4"]) * parseFloat(disks[i]["6"]);
		}
		console.log(disks);
		if (!this.systemData){
            return;
        }
		

		var halfHeight = this.getHeight()/2.0;
		var chartTop = halfHeight + this.graphTop1 * this.getHeight()/100.0; 
		
		var chartWidth = this.graphWidth / 12.0 * this.getWidth()/100.0;
		var chartLeft =  this.graphLeft * this.getWidth()/100.0 + chartWidth*2.0;
		var chartHeight = this.graphHeight * 0.6 * (this.getHeight()/100.0);

        if (box !== undefined && box !== null) {
            chartWidth = box.width / 4.0 * this.getWidth()/100.0;
            chartHeight = box.height * 0.7 * (this.getHeight()/100.0);
            chartTop = (box.top + this.graphMargin/2.0) * this.getHeight()/100.0; 
            chartLeft =  box.left * this.getWidth()/100.0;
        }
		
        this.diskColor = "rgb(175,175,150)";
        this.diskColorFill = "rgba(155,155,130, 0.7)";
        var diskColorText = "rgba(235,235,235, 1.0)";
        var chartArea = chartWidth * chartHeight;
        var K = chartArea/totalFree ;
        var diskTop = chartTop;
        var diskSquare = this.svg.selectAll(".diskSquare")
			.data(disks)
			.enter()
			.append("g")
			.attr("class", "diskSquare")
			.attr("id", function(d) {return d["1"];});

		diskSquare.append("rect")
			.attr("x", chartLeft)
			.attr("y", function(d, i) {
				return chartTop + i*0.5 * this.getHeight()/100.0 + d["cumulativeFree"] * K / chartWidth;}.bind(this))
			.attr("width", chartWidth)
			.attr("height", function(d, i) {
				var y = parseFloat(d["4"]) * parseFloat(d["5"]);
				var h = y * K / chartWidth;
				return h;
			})
			.style("stroke", this.diskColor); 
		diskSquare.append("rect")
			.attr("x", chartLeft)
			.attr("y", function(d, i) {
				return chartTop + i*0.5 * this.getHeight()/100.0 + d["cumulativeFree"] * K / chartWidth;}.bind(this))
			.attr("width", function(d, i) {
				return chartWidth * parseFloat(d["6"]) / parseFloat(d["5"]);})
			.attr("height", function(d, i) {
				var y = parseFloat(d["4"]) * parseFloat(d["5"]);
				var h = y * K / chartWidth;
				return h;
			})
			.style("fill", this.diskColorFill); 
		diskSquare.append("text")
			.attr("x", chartLeft - chartWidth*0.1)
			.attr("y", function(d, i) {
				var y = parseFloat(d["4"]) * parseFloat(d["5"]);
				var h = y * K / chartWidth;
				return chartTop + i*0.5 * this.getHeight()/100.0 + d["cumulativeFree"] * K / chartWidth;}.bind(this))
			.style("text-anchor", "end")
			.attr("fill", diskColorText)
			.attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
			.attr("dy", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	    
		    .text(function(d) { 
		    	var usedMB = parseInt(d["4"] * d["6"] / 1048576);
		    	var totalMB = parseInt(d["4"] * d["5"] / 1048576);
		    	return d["3"].split(" ")[0] + "   " + usedMB + " MB  /  " + totalMB + " MB"; });

		this.showText({x: chartLeft,
			y: chartTop - parseInt(this.fontScaleFactor * this.getWidth()),
			fontSize: parseInt(1.4*this.fontScaleFactor * this.getWidth()),
			anchor: "middle",
			text: "Disk Usage"});
		var physicalUsedGB = parseInt(physical["4"] / 1024)  * parseInt(physical["6"] / 1048576);
		var physicalTotalGB = parseInt(physical["4"]) * parseInt(physical["5"]) / parseFloat(1024*1048576);
		var virtualUsedMB = parseInt(virtual["4"] * virtual["6"] / 1048576);
		var virtualTotalMB = parseInt(virtual["4"] * virtual["5"] / 1048576);
		/*this.showText({x: chartLeft + 2.4 * chartWidth,
			y: chartTop + chartHeight + (disks.length-1)*this.getHeight()/100.0 - 0.5*parseInt(this.fontScaleFactor * this.getWidth()),
			fontSize: parseInt(1.2*this.fontScaleFactor * this.getWidth()),
			text: "Physical Memory: " + physicalUsedGB + " GB  /  " + physicalTotalGB + " GB",
			anchor:"end" });
		this.showText({x: chartLeft + 2.4 * chartWidth,
			y: chartTop  + chartHeight + (disks.length-1)*this.getHeight()/100.0 + parseInt(this.fontScaleFactor * this.getWidth()),
			fontSize: parseInt(1.2*this.fontScaleFactor * this.getWidth()),
			text: "Virtual Memory:  " + virtualUsedMB + " MB  /  " + virtualTotalMB + " MB",
			anchor: "end" });
		*/
	},
	drawApplicationCPUMEMPlot: function(){
		var programs = this.systemData.programList.map(function(d){return d.value;})
			.sort(function(a,b){ 
				if (b.cpu > a.cpu  || b.mem > a.mem){
					return 1;
				}
				else if (b.cpu < a.cpu  && b.mem < a.mem){
					return -1;
				}
				return 0;
			})
			.slice(0, this.state.numberOfApplications);
		var radius = this.graphHeight/4.0 * this.getHeight()/100.0;
		var chartLeft = (this.graphLeft + this.graphWidth*0.4) * this.getWidth()/100.0;
		var chartTop = this.graphTop1 *0.6 * this.getHeight()/100.0; 
		var chartWidth = this.graphWidth * 0.6 * this.getWidth()/100.0;
		var chartHeight = this.graphHeight *0.9 * this.getHeight()/100.0;
		var squaresLeft =  (this.graphLeft-2.0) * this.getWidth()/100.0;
		this.programColor = "rgb(95,80,105)";
		this.programColorSelected = "rgb(180,180,190)";
		this.programColorSelectedText = "rgb(50,50,40)";
		this.programColorText = "rgb(235,235,235)"
		this.programColorAlpha = "rgba(95,80,105,0.3)";
		this.programColorAlphaSelected = "rgba(180,180,190, 0.3)";

		// setup x 
		var xValue = function(d) { return d.cpu;}, // data -> value
		    xScale = d3.scale.linear().range([chartLeft, chartLeft + chartWidth]), // value -> display
		    xMap = function(d) { return xScale(xValue(d));}, // data -> display
		    xAxis = d3.svg.axis().scale(xScale).orient("bottom");

		// setup y
		var yValue = function(d) { return d.mem;}, // data -> value
		    yScale = d3.scale.linear().range([chartTop + chartHeight, chartTop]), // value -> display
		    yMap = function(d) { return yScale(yValue(d));}, // data -> display
		    yAxis = d3.svg.axis().scale(yScale).orient("left");

		// setup fill color
		/*var cValue = function(d) { return d.Manufacturer;},
		    color = d3.scale.category10();*/

		// don't want dots overlapping axis, so add in buffer to data domain
		xScale.domain([0, d3.max(programs, xValue)+1]);
		yScale.domain([0, d3.max(programs, yValue)+1]);

		// x-axis
		this.svg.append("g")
		    .attr("class", "axis")
		    .attr("fill", "rgb(220,220,220)")
			.attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
		    .attr("transform", "translate(" + 0 +","+ (chartTop + chartHeight) + ")")
		    .call(xAxis)
			.append("text")
			.attr("class", "label")
			.attr("x", chartLeft+chartWidth)
			.attr("y", -6)
			.style("text-anchor", "end")
			.text("CPU Time (S)");
			

		// y-axis
		this.svg.append("g")
		    .attr("class", "axis")
		    .attr("fill", "rgb(220,220,220)")
			.attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
			.attr("transform", "translate(" + chartLeft +","+ 0 + ")")
		    .call(yAxis)
			.append("text")
			.attr("class", "label")
			.attr("transform", "rotate(-90)")
			.attr("x", -50)
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text("Memory (MB)");

		// draw dots
		var dots = this.svg.selectAll(".dot")
		  	.data(programs)
			.enter()
			.append("g")
			.attr("class", "dot")
			.attr("id", function(d) {return d["1"];});

		dots.append("circle")
			.attr("r", this.getHeight()/200.0)
			.attr("cx", xMap)
			.attr("cy", yMap)
			.style("fill", function(d) {  return (d["1"] === this.selectedProgramNumber)? this.programColorSelected : this.programColor;}.bind(this));
		dots.append("circle")
			.attr("r", this.getHeight()/50.0)
			.attr("cx", xMap)
			.attr("cy", yMap)
			.style("fill", function(d) {  return (d["1"] === this.selectedProgramNumber)? this.programColorAlphaSelected : this.programColorAlpha;}.bind(this));


		var squareWidth = this.graphWidth / 6.0 * this.getWidth()/100.0;
		var squareHeight = this.graphHeight * 0.8 * (this.getHeight()/100.0) / (programs.length/2.0);
		var squareVerticalPadding = squareHeight / 10.0;
		var squareHorizontalPadding = squareWidth / 10.0;
		
		var programSquare = this.svg.selectAll(".programSquares")
			.data(programs)
			.enter()
			.append("g")
			.attr("class", "programSquares")
			.attr("id", function(d) {return d["1"];});

		programSquare.append("rect")
			.attr("x", function(d, i) {return squaresLeft + ((i%2 ===0 )? 0: squareWidth + squareHorizontalPadding);})
			.attr("y", function(d, i) {return chartTop + parseInt(i/2) * (squareHeight + squareVerticalPadding);})
			.attr("width", squareWidth)
			.attr("height", squareHeight)
			.style("fill", function(d) {  return (d["1"] === this.selectedProgramNumber)? this.programColorSelected : this.programColor;}.bind(this));
		programSquare.append("text")
			//.filter(function(d){return (d.Alias.split(" ").length > 1);})
			.attr("x", function(d, i) {return squaresLeft + ((i%2 ===0 )? squareWidth/2.0: 1.5*squareWidth + squareHorizontalPadding);})
			.attr("y", function(d, i) {return chartTop + squareHeight/2.0 + parseInt(i/2) * (squareHeight + squareVerticalPadding);})
			.style("text-anchor", "middle")
			.attr("fill", function(d) {  return (d["1"] === this.selectedProgramNumber)? this.programColorSelectedText : this.programColorText;}.bind(this))
			.attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
			.attr("dy", parseInt(0.5*this.fontScaleFactor * this.getWidth()) + "px")	    
		    .text(function(d) { return d["2"]; });

		this.svg.append("g")
			.attr("class", "instruction")
			.append("text")
			.attr("x", this.getWidth()/2.0)
			.attr("y", chartHeight + 1.8*chartTop)
			.style("text-anchor", "middle")
			.attr("fill", "rgb(235,235,235)")
			.attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
		    .text("Click on program name to highlight it in the plot and vice versa");

		this.showText({x: squaresLeft, y: 0.8*chartTop, fontSize: parseInt(1.4*this.fontScaleFactor * this.getWidth()),
			text: "Programs"});
	},
	showText: function(textData) {
		textData.class = textData.class || "heading";
		textData.anchor = textData.anchor || "start";
		textData.fill = textData.fill || "rgb(235,235,235)";
		this.svg.append("g")
			.attr("class", textData.class)
			.append("text")
			.attr("x", textData.x)
			.attr("y", textData.y)
			.style("text-anchor", textData.anchor)
			.attr("fill", textData.fill)
			.attr("font-size", textData.fontSize + "px")
		    .text(textData.text);
	},
	checkProgramClicked: function(position, date){
		var x = position.x;
		var y = position.y;
		var svg = this.svg;
		var selectedProgramNumber = 0;
		var programColor = this.programColor;
		var programDots = this.svg.selectAll(".dot")
		var programSquares = this.svg.selectAll(".programSquares")
			.each(function(d){
				var box = this.firstChild.getBBox();
				if ((x >= box.x && x <= box.x + box.width) && (y >= box.y && y <= box.y + box.height)){
					selectedProgramNumber = parseInt(this.id);
				}
			});

		programDots.each(function(d){
			var box = this.lastChild.getBBox();
			if ((x >= box.x && x <= box.x + box.width) && (y >= box.y && y <= box.y + box.height)){
				selectedProgramNumber = parseInt(this.id);
			}
		});
		if (selectedProgramNumber > 0){
			this.selectedProgramNumber = selectedProgramNumber;
			this.eventTriggeredRefresh(date);
		}
		
	},

	checkNodeButtonClicked: function(position, date){
		var x = position.x;
		var y = position.y;
		var svg = this.svg;
		var selectedNodeAlias = null;

	    var box;
		var nodeButtons = this.svg.selectAll(".nodeButton")
			.each(function(d){
				box = this.firstChild.getBBox();
 				if ((x >= box.x && x <= box.x + box.width) && (y >= box.y && y <= box.y + box.height) && selectedNodeAlias === null){
					selectedNodeAlias = this.id;
  					console.log("selectedNodeAlias: " + selectedNodeAlias);
				}
			});

		if (selectedNodeAlias !== null){
			this.displayDataElementName = selectedNodeAlias;
            this.navigationHandle.prune();
            this.navigationHandle.addItemAndGoForward(selectedNodeAlias);
            //this.autoClickMenuNodeButton(selectedNodeAlias);
			this.eventTriggeredRefresh(date);
		}
		
	},

  /*  autoClickMenuNodeButton: function(nodeAlias)
    {
        for (var key in this.buttons)
        {
            var button = this.buttons[key];
            if (button.nodeAlias === nodeAlias)
            {
                button.mouseUp();
                break;
            }
        }
    },*/

	drawProcessorLoad: function(box){
        if (!this.systemData){
            return;
        }
		var processors = this.systemData.processorTable
			.sort(function(a,b){ return parseInt(b.key) - parseInt(a.key);})
			.map(function(d){return d.value;});

		
		var halfHeight = this.getHeight()/2.0;
		var chartTop = halfHeight + this.graphTop1 * this.getHeight()/100.0; 
		var squaresLeft =  (this.graphLeft + this.graphWidth*0.50) * this.getWidth()/100.0;
		var squareWidth = this.graphWidth / 15.0 * this.getWidth()/100.0;
		var squareHeight = this.graphHeight * 0.6 * (this.getHeight()/100.0) / (processors.length/2.0);
        var squareVerticalPadding = squareHeight / 10.0;
        var squareHorizontalPadding = squareWidth / 10.0;
        if (box !== undefined && box !== null) {
            squareWidth = box.width / 4.0 * this.getWidth()/100.0;
            squareHeight = box.height * 0.7 * (this.getHeight()/100.0) / (processors.length/2.0);
            squareVerticalPadding = squareHeight / 10.0;
            squareHorizontalPadding = squareWidth / 10.0;
            chartTop = (box.top + this.graphMargin/2.0) * this.getHeight()/100.0; 
            squaresLeft =  (box.left+box.width*0.5) * this.getWidth()/100.0 - squareWidth - 1.5*squareHorizontalPadding;
        }
		
        this.processorColor = "rgb(175,175,150)";
        this.processorColorFill = "rgba(155,155,130, 0.7)";
        var processorColorText = "rgba(155,155,180, 1.0)";

		this.svg.append("g")
			.append("rect")
			.attr("x", squaresLeft - squareHorizontalPadding)
			.attr("y", chartTop + 3 * squareVerticalPadding)
			.attr("width", 2.0 * squareWidth + 3.0 *squareHorizontalPadding )
			.attr("height", (processors.length/2.0) * (squareHeight + squareVerticalPadding) + squareVerticalPadding)
			.style("stroke", this.processorColor);

		var processorSquares = this.svg.selectAll(".processors")
			.data(processors)
			.enter()
			.append("g")
			.attr("class", "processors");

		processorSquares.append("rect")
			.attr("x", function(d, i) {return squaresLeft + ((i%2 ===0 )? 0: squareWidth + squareHorizontalPadding);})
			.attr("y", function(d, i) {return chartTop + 4*squareVerticalPadding + parseInt(i/2) * (squareHeight + squareVerticalPadding);})
			.attr("width", squareWidth)
			.attr("height", squareHeight)
			.style("stroke", this.processorColor);

		processorSquares.append("rect")
			.attr("x", function(d, i) {return squaresLeft + ((i%2 ===0 )? 0: squareWidth + squareHorizontalPadding);})
			.attr("y", function(d, i) {return chartTop + 4*squareVerticalPadding + parseInt(i/2) * (squareHeight + squareVerticalPadding) + ((1.0 - d["2"]/100.0) * squareHeight);})
			.attr("width", squareWidth)
			.attr("height", function(d){ return d["2"]/100.0 * squareHeight;})
			.style("fill", this.processorColorFill);

		processorSquares.append("text")
			//.filter(function(d){return (d.Alias.split(" ").length > 1);})
			.attr("x", function(d, i) {return squaresLeft + ((i%2 ===0 )? squareWidth/2.0: 1.5*squareWidth + squareHorizontalPadding);})
			.attr("y", function(d, i) {return chartTop + 4*squareVerticalPadding + squareHeight/2.0 + parseInt(i/2) * (squareHeight + squareVerticalPadding);})
			.style("text-anchor", "middle")
			.attr("fill", processorColorText)
			.attr("font-size", parseInt(this.fontScaleFactor * this.getWidth()) + "px")	
			.attr("dy", parseInt(0.5*this.fontScaleFactor * this.getWidth()) + "px")	    
		    .text(function(d) { return d["2"] + "%"; });
		this.showText({x: squaresLeft + (box ? squareWidth+squareHorizontalPadding*0.5 : -squareHorizontalPadding),
			y: chartTop + squareVerticalPadding, anchor: box? "middle":"start",
			fontSize: box? parseInt(2.0*this.fontScaleFactor * this.getWidth()) : parseInt(1.4*this.fontScaleFactor * this.getWidth()),
			text: box ? "System":"CPU Processor Load"});
	},
	drawRAMStatus: function(){
		var totalRAM = this.systemData.systemMemory;
		var memoryUsage = this.systemData.memoryUsage["total"];
		var memEmptyPercent = 100 - parseInt(0.8 + 100.0 * memoryUsage/totalRAM);
		//console.log("memEmptyPercent: " + memEmptyPercent);
		var centArray = new Array(100);
		centArray = centArray.map(function(d,i){return i;});
		var chartLeft = (this.graphLeft + this.graphWidth*0.75) * this.getWidth()/100.0;
		var halfHeight = this.getHeight()/2.0;
		var chartTop = halfHeight + this.graphTop1 * this.getHeight()/100.0; 
		var chartWidth = this.graphWidth * 0.4 * this.getWidth()/100.0;
		var chartHeight = this.graphHeight *0.6 * this.getHeight()/100.0;
		var squaresLeft =  chartLeft;
		this.RAMColorEmpty = "rgb(0,0,0)";
		this.RAMColorFilled = "rgb(155,155,175)";
		
		var squareWidth = this.graphWidth / 50.0 * this.getWidth()/100.0;
		var squareHeight = this.graphHeight * 0.62 * (this.getHeight()/100.0) / (centArray.length/10.0);
		var squareVerticalPadding = squareHeight / 10.0;
		var squareHorizontalPadding = squareWidth / 8.0;

		this.svg.append("g")
			.append("rect")
			.attr("x", squaresLeft - squareHorizontalPadding)
			.attr("y", chartTop - squareVerticalPadding)
			.attr("width", 10.0 * squareWidth + 11.0 *squareHorizontalPadding )
			.attr("height", (centArray.length/10.0) * (squareHeight + squareVerticalPadding) + squareVerticalPadding)
			.style("stroke", this.RAMColorFilled);
	
		
		var memoryPieces = this.svg.selectAll(".memPiece")
			.data(centArray)
			.enter()
			.append("g")
			.attr("class", "memPiece");

		memoryPieces.append("rect")
			.attr("x", function(d, i) {return squaresLeft + ((i%10) * (squareWidth + squareHorizontalPadding));})
			.attr("y", function(d, i) {return chartTop + (parseInt(i/10) * (squareHeight + squareVerticalPadding));})
			.attr("width", squareWidth)
			.attr("height", squareHeight)
			.attr("stroke", this.RAMColorFilled)
			.attr("fill", function(d, i){ return (i < memEmptyPercent) ? this.RAMColorEmpty: this.RAMColorFilled;}.bind(this));

		this.showText({x: squaresLeft - squareHorizontalPadding,
			y: chartTop - 6.0*squareVerticalPadding,
			fontSize: parseInt(1.4*this.fontScaleFactor * this.getWidth()),
			text: "Memory (MB): " + memoryUsage + "/" + totalRAM});
	},
	drawSAGE2AppsEventHistory: function(date, box) {
		var eventHistory = this.appMonitor.getEventHistory();
		if (eventHistory.length === 0 ){
			this.displayNoDataMessage({left:box.left || this.graphAreaLeft, width:box.width || this.graphAreaWidth, 
                top: box.top || this.graphTopSingle, height:box.height || this.graphHeightSingle, message: "Event Log"});
			return ;
		}

		var graphHeight = this.graphAreaHeight - 10.0;
		var graphTop = this.graphAreaTop + 10.0;
		var graphWidth = this.graphAreaWidth - 10.0;
		var graphLeft = this.graphAreaLeft + 7.0;
        if (box !== undefined && box !== null) {
            graphHeight = box.height - 10.0;
            graphTop = box.top + 10.0;
            graphWidth = box.width - 10.0;
            graphLeft = box.left + 7.0;
        }
		var appsPresent = d3.set(eventHistory.map(function(d) { return d.id;})).values();
		appsPresent.sort();
		var usersPresent = d3.set(eventHistory.map(function(d) { return d.user.color;})).values();
		var appUsersPresent = [];
		for (var i = 0; i < appsPresent.length; i++) {
		    for (var l = 0; l < usersPresent.length; l++) {
		        appUsersPresent.push({
		        	app:appsPresent[i], 
		        	userColor:usersPresent[l]
		        });
		    }
		}
		

		var numberOfAppsPresent = appsPresent.length;
		var numberOfUsersPresent = usersPresent.length;
		var numberOfAppUsersPresent = appUsersPresent.length;
		//console.log(eventHistory);
		var heightOfAppSpace = Math.min(8.0, graphHeight / appsPresent.length);
		var heightOfUserSpace = Math.min(1.0, heightOfAppSpace / numberOfUsersPresent);


		var yScale1 = d3.scale.ordinal()
		    .domain(appsPresent)
		    .range(Object.keys(appsPresent));
		var yScale2 = d3.scale.ordinal()
		    .domain(usersPresent)
		    .range(Object.keys(usersPresent));
		var appContainer = this.svg.selectAll(".appContainers")
			.data(appsPresent)
			.enter()
			.append("g")
			.attr("class", "appContainers");

		appContainer.append("rect")
			.attr("x", graphLeft * this.getWidth()/100.0)
			.attr("y", function(d, i){
				var ht = graphTop + (graphHeight - heightOfAppSpace*numberOfAppsPresent)/2.0 + i*heightOfAppSpace;
				return ht/100.0 * this.getHeight();
			}.bind(this))
			.attr("width", graphWidth/100.0 * this.getWidth())
			.attr("height", heightOfAppSpace/100.0 * this.getHeight())
			.attr("fill", "rgba(0,0,0,0)")
			.attr("stroke", "rgba(190,190,190,1.0)");

		appContainer.append("text")
			.attr("class", "label")
			.attr("transform", function(d, i){
				var ht = (graphTop + (graphHeight - heightOfAppSpace*numberOfAppsPresent)/2.0 + (i+0.5)*heightOfAppSpace)/100.0 * this.getHeight();
				return "translate("+ (0.5 + graphLeft) * this.getWidth()/100.0 +","+ ht +")rotate(-90)";
			}.bind(this))
			.attr("dy", ".71em")
			.style("text-anchor", "middle")
			.attr("font-size", this.fontScaleFactor*this.getWidth() + "px")
			.attr("fill", "rgb(220,220,220)")
			.text(function(d) {return d;});

		var appUsers = this.svg.selectAll(".appUsersPresent")
			.data(appUsersPresent)
			.enter()
			.append("g")
			.attr("class", "appUsersPresent");

		appUsers.append("rect")
			.attr("x", (graphLeft+3.0) * this.getWidth()/100.0)
			.attr("y", function(d) {
				var ht = graphTop + (graphHeight - heightOfAppSpace*numberOfAppsPresent)/2.0 + yScale1(d.app)*heightOfAppSpace;
				var ht2 = (heightOfAppSpace - heightOfUserSpace*numberOfUsersPresent)/2.0 + yScale2(d.userColor)*heightOfUserSpace;
				return (ht + ht2)/100.0 * this.getHeight();
			}.bind(this))
			.attr("width", 0.03 * this.getWidth())
			.attr("height", heightOfUserSpace /100.0 * this.getHeight())
			.attr("fill", function(d){return d.userColor;})
			.attr("stroke", "rgba(0,0,0,1.0)");
		var historyDate = date - (1000*this.historyLimit/this.state.pullRate);
		var eventTypeScale = d3.scale.category10()
			.domain(["pointerScroll", "pointerMove", "pointerPress", "pointerRelease", "widgetEvent", "keyboard", "specialKey"]);
	    var xScale = d3.time.scale()
			.domain([historyDate, date])
		   	.range([(graphLeft+6.5)/100.0 * this.getWidth(), (graphLeft + graphWidth)/100.0 * this.getWidth()]);
		//console.log(xScale.domain());
		
		var eventBits = this.svg.selectAll(".eventBit")
			.data(eventHistory)
			.enter()
			.append("g")
			.attr("class", "eventBit");
		eventBits.append("rect")
			.attr("width", this.getWidth()/100.0)
			.attr("height", heightOfUserSpace* this.getHeight()/100.0)
			.attr("fill", function(d){ return eventTypeScale(d.type);})
			//.attr("stroke", "rgba(0,0,0,1.0)")
			.attr("x",function(d) { return xScale(d.date);})
			.attr("y", function(d) {
				var ht = graphTop + (graphHeight - heightOfAppSpace*numberOfAppsPresent)/2.0 + yScale1(d.id)*heightOfAppSpace;
				var ht2 = (heightOfAppSpace - heightOfUserSpace*numberOfUsersPresent)/2.0 + yScale2(d.user.color)*heightOfUserSpace;
				return (ht + ht2)/100.0 * this.getHeight();
			}.bind(this));

		var userContainer = this.svg.append("g");
		userContainer.append("rect")
			.attr("x", (graphLeft+2.5) * this.getWidth()/100.0)
			.attr("y", (graphTop + (graphHeight - heightOfAppSpace*numberOfAppsPresent)/2.0)/100.0 * this.getHeight())
			.attr("width", 0.040 * this.getWidth())
			.attr("height", (heightOfAppSpace*numberOfAppsPresent)/100.0 * this.getHeight())
			.attr("fill", "rgba(0,0,0,0)")
			.attr("stroke", "rgba(190,190,190,1.0)");

		userContainer.append("text")
			.attr("x", (graphLeft+4.5) * this.getWidth()/100.0)
			.attr("y", (graphTop -2.0 + (graphHeight - heightOfAppSpace*numberOfAppsPresent)/2.0)/100.0 * this.getHeight())
			.style("text-anchor", "middle")
			.attr("font-size", this.fontScaleFactor*this.getWidth() + "px")
			.attr("fill", "rgb(220,220,220)")
			.text("User");
		var heading = this.svg.append("g");
		heading.append("text")
			.attr("x", (box? box.left + 0.5*box.width : graphLeft+2.0) * this.getWidth()/100.0)
			.attr("y", (box? box.top + 0.5*this.graphMargin : graphTop+2.0)/100.0 * this.getHeight())
			.style("text-anchor", box ? "middle" : "start")
			.attr("font-weight", "bold")
            .attr("font-size", parseInt(2.0*this.fontScaleFactor * this.getWidth()) + "px")
			.attr("fill", "rgb(220,220,220)")
			.text("Event Log");

		var xAxis = d3.svg.axis()
          	.scale(xScale)
          	.orient("bottom")
          	.innerTickSize(-(heightOfAppSpace*numberOfAppsPresent)/100.0 * this.getHeight())
            .outerTickSize(0)
            .tickPadding(5)
          	.ticks(8);

        this.svg.append("g")
            .attr("class", "axis")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", "0.5rem")
            .attr("transform", "translate(" + 0 +","+ ((graphTop + (graphHeight + heightOfAppSpace*numberOfAppsPresent)/2.0) *this.getHeight()/100.0) + ")")
            .call(xAxis)
            .append("text")
			.attr("class", "label")
			.attr("x", (graphLeft + graphWidth/2.0)/100.0 * this.getWidth())
			.attr("y", 3.0 * this.fontScaleFactor * this.getWidth())
			.attr("font-size", this.fontScaleFactor * this.getWidth() + "px")
			.style("text-anchor", "end")
			.text("Clock time");
        if (!box) {
            this.addLegend("individual", eventTypeScale.domain(), eventTypeScale, {y: graphTop+1.0, x: graphLeft + graphWidth}, 0.5);    
        }
		
	},
	drawSAGE2AppsEventTrigger: function() {

	},
	wipeItAll: function(){
		this.svg.selectAll("g").remove();
		this.svg.selectAll(".lineIn").remove();
		this.svg.selectAll(".lineOut").remove();
		this.svg.selectAll(".dot").remove();
		this.svg.selectAll(".processors").remove();
	},
    drawHome: function(date) {
        var clickableAreaList = [{id: "Network", left: this.graphAreaLeft, top: this.graphAreaTop, 
                                width: this.graphAreaWidth/2.0, height: this.graphAreaHeight/2.0},
                                {id: "SAGE2Apps", left: this.graphAreaLeft + this.graphAreaWidth/2.0, top: this.graphAreaTop, 
                                width:this.graphAreaWidth/2.0, height:this.graphAreaHeight/2.0},
                                {id: "system", left:this.graphAreaLeft,top:this.graphAreaTop + this.graphAreaHeight/2.0, 
                                width:this.graphAreaWidth/2.0, height:this.graphAreaHeight/2.0},
                                {id: "energy", left:this.graphAreaLeft + this.graphAreaWidth/2.0,top:this.graphAreaTop + this.graphAreaHeight/2.0, 
                                width:this.graphAreaWidth/2.0, height:this.graphAreaHeight/2.0}];
        if(this.NetworkNode !== null) {
            var showData = this.searchNode(this.NetworkNode,"Network");
            this.drawSummaryChart(showData, {left: this.graphAreaLeft + this.graphAreaWidth/4.0, top: this.graphAreaTop, 
                height:this.graphAreaHeight/2.0 - 8, width:this.graphAreaWidth/2.0, small:true});
        }
        this.drawSAGE2AppsEventHistory(date, clickableAreaList[1]);
        
        this.drawSystemData(clickableAreaList[2]);
        this.drawEnergyChart(clickableAreaList[3]);
        
        var clickableSquares = this.svg.selectAll(".clickable")
            .data(clickableAreaList)
            .enter()
            .append("g")
            .attr("class", "clickable")
            .attr("id", function(d) {return d.id;});

        clickableSquares.append("rect")
            .attr("id", function(d) {return d.id;})
            .attr("x", function(d, i) {return d.left * this.getWidth()/100.0;}.bind(this))
            .attr("y", function(d, i) {return d.top * this.getHeight()/100.0;}.bind(this))
            .attr("width", function(d, i) {return d.width * this.getWidth()/100.0 - 2.0;}.bind(this))
            .attr("height", function(d, i) {return d.height * this.getHeight()/100.0 - 2.0;}.bind(this))
            //.style("stroke", "rgba(200,180,175,1.0)")
            .style("fill", "rgba(200,180,175,0.3)");
    },
    checkHomeScreenClickablesClicked: function(position, date){
        var x = position.x;
        var y = position.y;
        var svg = this.svg;
        var selectedDisplayName = null;

    
        var nodeButtons = this.svg.selectAll(".clickable")
            .each(function(d){
                var box = this.firstChild.getBBox();
                if ((x >= box.x && x <= box.x + box.width) && (y >= box.y && y <= box.y + box.height) && selectedDisplayName === null){
                    selectedDisplayName = this.id;
                    console.log("selectedDisplayName: " + selectedDisplayName);
                }
            });

        
        if (selectedDisplayName !== null){
            this.displayDataElementName = selectedDisplayName;
            this.navigationHandle.reset();
            this.navigationHandle.addItemAndGoForward(selectedDisplayName);
            //this.autoClickMenuNodeButton(selectedNodeAlias);
            this.eventTriggeredRefresh(date);
        }
        
    },

    drawEnergyChart: function(box) {
        if (!this.energyData) {
            return;
        }
        this.showLargeChart();
        
        //console.log(this.energyData.max);
        var chartTop = this.graphAreaTop;
        var chartLeft = this.graphAreaLeft;
        var chartHeight = this.graphAreaHeight;
        var chartWidth = this.graphAreaWidth;
        if (box) {
            chartTop = box.top + this.graphMargin/2.0;
            chartLeft = box.left;
            chartHeight = box.height;
            chartWidth = box.width;
        }

        var n = this.energyData.data.length;
        var r = (chartWidth/(3.0 * n + 1));
        if (r > chartHeight * 0.30) {
            r = chartHeight * 0.30;
        }
        var w = 2*r/5;
        var h = chartHeight/3.5;
        var totalInputPowerColor = "rgba(78,65,180,1.0)";
        var pdus = this.svg.selectAll(".pdu")
            .data(this.energyData.data)
            .enter()
            .append("g")
            .attr("class", "pdu");


        pdus.append("circle")
            .attr("r", r * this.getWidth()/100.0)
            .attr("cx", function(d,i) { return (chartLeft  + 2.5*r + i*3.0*r) * this.getWidth()/100.0 ;}.bind(this))
            .attr("cy", (chartTop + chartHeight*0.5) * this.getHeight()/100.0)
            .style("fill", "rgba(180,210,210,0.7)");
        pdus.append("text")
            .attr("x", function(d,i) { return (chartLeft  + 2.5*r + i*3.0*r) * this.getWidth()/100.0 ;}.bind(this))
            .attr("y", (chartTop + chartHeight*0.5) * this.getHeight()/100.0)
            .style("text-anchor", "middle")
            .attr("font-weight", "bold")
            .attr("font-size", 2.0*this.fontScaleFactor*(chartWidth /100.0  * this.getWidth()) + "px")
            .attr("fill", "rgb(30,30,30)")
            .text(function (d) { return d.accumulatedEnergy + " kWh"});
        var k = 0;
        for (k=0; k<this.energyData.data[0].branchCurrent.length;k++) {
            pdus.append("rect")
                .attr("x", function(d,i) { return (chartLeft + 1.5*r + i*3.0*r + 2*k*w) * this.getWidth()/100.0 ;}.bind(this))
                .attr("y", (chartTop + chartHeight*0.5 - 2.0*r - h) * this.getHeight()/100.0)
                .attr("width", w * this.getWidth()/100.0)
                .attr("height", h * this.getHeight()/100.0)
                .attr("stroke", "rgba(128,120,180,1.0)")
                .attr("fill", "rgba(255,255,255,0.0)");
            pdus.append("rect")
                .attr("x", function(d,i) { return (chartLeft  + 1.5*r + i*3.0*r + 2*k*w)* this.getWidth()/100.0;}.bind(this))
                .attr("y", function(d) {
                    return (chartTop + chartHeight*0.5 - 2.0*r - (h * d.branchCurrent[k]/this.energyData.max.current)) * this.getHeight()/100.0;
                }.bind(this))
                .attr("width", w * this.getWidth()/100.0)
                .attr("height", function(d) {
                    return (h * d.branchCurrent[k]/this.energyData.max.current) * this.getHeight()/100.0;
                }.bind(this))
                .attr("stroke", "rgba(255,255,255,0.0)")
                .attr("fill", "rgba(128,120,180,1.0)");
        }

        pdus.append("rect")
                .attr("x", function(d,i) { return (chartLeft  + 2.5*r + i*3.0*r - w) * this.getWidth()/100.0 ;}.bind(this))
                .attr("y", (chartTop + chartHeight*0.5 + 2.0*r) * this.getHeight()/100.0)
                .attr("width", 2.0*w * this.getWidth()/100.0)
                .attr("height", 0.75*h * this.getHeight()/100.0)
                .attr("stroke", totalInputPowerColor)
                .attr("fill", "rgba(255,255,255,0.0)");
        pdus.append("rect")
                .attr("x", function(d,i) { return (chartLeft  + 2.5*r + i*3.0*r - w)* this.getWidth()/100.0;}.bind(this))
                .attr("y", function(d) {
                    return (chartTop + chartHeight*0.5 + 2.0*r) * this.getHeight()/100.0;
                }.bind(this))
                .attr("width", 2.0*w * this.getWidth()/100.0)
                .attr("height", function(d) {
                    return (0.75*h * d.totalInputPower/this.energyData.max.totalInputPower) * this.getHeight()/100.0;
                }.bind(this))
                .attr("stroke", "rgba(255,255,255,0.0)")
                .attr("fill", totalInputPowerColor);
        this.svg.append("g")
            .attr("class", "heading")
            .append("text")
            .attr("x", (chartLeft + chartWidth*0.5)/100.0 * this.getWidth())
            .attr("y", (box? box.top + 0.5*this.graphMargin : chartTop+2.0)/100.0 * this.getHeight())
            .attr("dy", parseInt(0.5*this.fontScaleFactor * this.getWidth()) + "px")
            .style("text-anchor", "middle")
            .attr("font-weight", "bold")
            .attr("font-size", parseInt(2.0*this.fontScaleFactor * this.getWidth()) + "px")
            .attr("fill", "rgb(220,220,220)")           
            .text("Energy");
        if (box) {
            return;
        }
        var yScale = d3.scale.linear()
            .domain([this.energyData.max.current,0])
            .range([(chartTop + chartHeight*0.5 - 2.0*r - h) * this.getHeight()/100.0, 
                (chartTop + chartHeight*0.5 - 2.0*r) * this.getHeight()/100.0]);
        var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient("left")
            //.innerTickSize(-chartWidth/100.0 * this.getWidth())
            .outerTickSize(5)
            .tickPadding(5)
            .ticks(5);
        this.svg.append("g")
            .attr("class", "axis")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", "0.5rem")
            .attr("transform", "translate(" + ((chartLeft + r) *this.getWidth()/100.0) +","+ 0 + ")")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("x", -(chartTop + chartHeight*0.5 - 2.0*r - h) * this.getHeight()/100.0)
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("Current (A)");
        var yScale2 = d3.scale.linear()
            .domain([0,this.energyData.max.totalInputPower])
            .range([(chartTop + chartHeight*0.5 + 2.0*r) * this.getHeight()/100.0, 
                (chartTop + chartHeight*0.5 + 2.0*r + 0.75*h) * this.getHeight()/100.0]);
        var yAxis2 = d3.svg.axis()
            .scale(yScale2)
            .orient("left")
            //.innerTickSize(-chartWidth/100.0 * this.getWidth())
            .outerTickSize(5)
            .tickPadding(5)
            .ticks(5);
        this.svg.append("g")
            .attr("class", "axis")
            .attr("fill", "rgb(220,220,220)")
            .attr("font-size", "0.5rem")
            .attr("transform", "translate(" + ((chartLeft + r) *this.getWidth()/100.0) +","+ 0 + ")")
            .call(yAxis2)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("x", -(chartTop + chartHeight*0.5 + 2.0*r + 0.75*h)*this.getHeight()/100.0)
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "start")
            .text("Input Power (W)");
        this.svg.append("g")
            .attr("class", "heading")
            .append("text")
            //.attr("x", (chartLeft + r)/100.0 * this.getWidth())
            //.attr("y", (chartTop + chartHeight*0.5) * this.getHeight()/100.0)
            .style("text-anchor", "middle")
            .attr("fill", "rgb(200,190,210)")
            .attr("font-weight", "bold")
            .attr("font-size", parseInt(1.8*this.fontScaleFactor * this.getWidth()) + "px") 
            .attr("transform", "translate(" +(chartLeft + r)/100.0 * this.getWidth() + "," + (chartTop + chartHeight*0.5) * this.getHeight()/100.0 + ")rotate(-90)")
            .text("Accumulated Energy");
    },
    drawNavigationButtons: function() {
        var navButtonList = ["Home", "Back", "Forward"];
        var navButtonColor = "rgba(200,180,175,0.3)";
        var navButtonTextColorActive = "rgba(230,230,230,1.0)";
        var navButtonTextColorInActive = "rgba(60,60,60,1.0)";
        var navigationButton = this.svg.selectAll(".navigationButton")
            .data(navButtonList)
            .enter()
            .append("g")
            .attr("class", "navigationButton");
        
        navigationButton.attr("id", function(d){ return d;})
            .append("rect")
            .attr("x", function(d, i){ return this.graphAreaLeft + i * 8 + "%"; }.bind(this))
            .attr("y", 0.5 + "%")
            .attr("width", 7.5 + "%")
            .attr("fill", navButtonColor)
            .attr("stroke", navButtonColor)
            .attr("height", "4%");


        navigationButton.append("text")
            .attr("x", function(d, i){ return this.graphAreaLeft +  4 + i*8 + "%"; }.bind(this))
            .attr("y", "3%")
            .style("text-anchor", "middle")
            .attr("fill", "rgb(235,235,235)")
            .attr("font-size", parseInt(2.0*this.fontScaleFactor * this.getWidth()) + "px")
            .text(function(d) { return d;});

    },
    checkNavigationButtonClicked: function(position, date){
        var x = position.x;
        var y = position.y;
        var svg = this.svg;
        var navButtonClicked = null;

        var box;
        var navButtons = this.svg.selectAll(".navigationButton")
            .each(function(d){
                box = this.getBBox();
                if ((x >= box.x && x <= box.x + box.width) && (y >= box.y && y <= box.y + box.height) && navButtonClicked === null){
                    navButtonClicked = this.id;
                }
            });

        switch (navButtonClicked) {
            case "Home":
                this.displayDataElementName = "home";
                this.navigationHandle.reset();
                this.eventTriggeredRefresh(date);
                break;
            case "Back":
                this.displayDataElementName = this.navigationHandle.goBackward();
                this.eventTriggeredRefresh(date);
                break;
            case "Forward":
                this.displayDataElementName = this.navigationHandle.goForward();
                this.eventTriggeredRefresh(date);
                break;
            default:
                break;
        }
    },

    slideShow:function() {
        var list = ["home", "Network", "switch1", "energy"];

        this.displayDataElementName = list[this.slideShowCounter];
        if (this.slideShowCounter < list.length){
            this.slideShowCounter = this.slideShowCounter + 1;
        } else {
            this.slideShowCounter = 0;
        }
    }

});
